import 'package:flutter/material.dart';
import 'package:flutter_login/page/home_page.dart';

class ReportLoginPage extends StatelessWidget {
  String email,password;

  ReportLoginPage(
      {Key? key, required this.email, required this.password})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellow[100],
      body: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
              Text(
                "Welcome ",
                style: TextStyle(
                    fontSize: 37,
                    fontWeight: FontWeight.w900,
                    color: Colors.purple,
                ),
              ),

            Padding(
              padding: const EdgeInsets.only(bottom: 5,top: 30),
              child: Text(
                'Email : $email',
                style: TextStyle(
                  fontSize: 20,fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 30),
              child: SizedBox(
                width: 250,
                height: 40,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary:Colors.purple,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12), // <-- Radius
                    ),
                  ),
                  child: Text("Next",style: TextStyle(fontSize: 20),),
                  onPressed: (){
                    Navigator.push(context, MaterialPageRoute(
                        builder: (context){
                          return HomePage(title: 'source');
                        })
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
