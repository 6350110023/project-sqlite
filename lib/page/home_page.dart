import 'package:flutter/material.dart';
import 'package:flutter_login/page/medicine/list_profile.dart';
import 'package:flutter_login/src/home.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<HomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<HomePage> {
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellow[100],
      appBar: AppBar(
        title: Text(widget.title),
        backgroundColor: Colors.purple.shade300,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: CircleAvatar(
              radius: 80,
              backgroundImage: AssetImage("assets/images/ya2jpg.jpg"),
            ),
          ),
          Text("ยา ตามกฎหมายว่ายา หมายถึง \n (๑) วัตถุที่รับรองไว้ในตำรายาที่รัฐมนตรีว่าการกระทรวงสาธารณสุขประกาศ \n(๒) วัตถุที่มุ่งหมายสำหรับใช้ในการวินิจฉัย บำบัด บรรเทา รักษา \n หรือป้องกันโรคหรือความเจ็บป่วยของมนุษย์หรือสัตว์ \n (๓) วัตถุที่เป็นเภสัชเคมีภัณฑ์หรือเภสัชเคมีภัณฑ์กึ่งสำเร็จรูป \n (๔) วัตถุที่มุ่งหมายสำหรับให้เกิดผลแก่สุขภาพ "),

        ],
      ),
      drawer: Drawer(
          backgroundColor: Colors.yellow[100],
          child: ListView(

            padding: EdgeInsets.zero,
            children: [
              UserAccountsDrawerHeader(
                accountName: const Text('Pharmacy'),
                accountEmail: const Text('-182-425-35-5-'),
                currentAccountPicture: CircleAvatar(
                  backgroundImage: AssetImage('assets/images/ya1.jpg'),
                ),
              ),
              ListTile(
                title: const Text('Home' ,style: TextStyle(fontWeight: FontWeight.w900),),
                leading: const Icon(Icons.home),
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                        builder: (context) => Homepage(title: 'Home')),
                  );
                },
              ),
              ListTile(
                title: const Text('medicine',style: TextStyle(fontWeight: FontWeight.w900),),
                leading: const Icon(Icons.add_box),
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                        builder: (context) =>
                            ListMedicine(title: 'Add Medicine')),
                  );
                },
              ),
            ],
          )),
    );
  }
}
