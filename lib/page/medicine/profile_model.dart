
class ProfileModel {
  int? id;
  String Midicine;
  String Properties;
  String image;

  ProfileModel({
    this.id,
    required this.Midicine,
    required this.Properties,
    required this.image,

  });

  factory ProfileModel.fromMap(Map<String, dynamic> json) =>
      new ProfileModel(
        id: json['id'],
        Midicine: json['firstname'],
        Properties: json['lastname'],
        image: json['image'],
      );

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'firstname': Midicine,
      'lastname': Properties,
      'image': image,
    };
  }
}