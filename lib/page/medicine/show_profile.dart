
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_login/page/medicine/database/database_helper.dart';
import 'package:flutter_login/page/medicine/profile_model.dart';

class ShowProfile extends StatelessWidget {
  final id;
  ShowProfile({Key? key, required this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.purple.shade300,
        title: Text('Detail Profile'),

      ),
      body: Center(
        child: FutureBuilder<List<ProfileModel>>(
            future: DatabaseHelper.instance.getProfile(this.id),
            builder: (BuildContext context,
                AsyncSnapshot<List<ProfileModel>> snapshot) {
              if (!snapshot.hasData) {
                return Center(child: Text('Loading...'));
              }
              return snapshot.data!.isEmpty
                  ? Center(child: Text('No Groceries in List.'))
                  : ListView(
                children: snapshot.data!.map((profile) {
                  return Center(
                    child: Column(
                      children: [
                        CircleAvatar(
                          backgroundImage: FileImage(File(profile.image)),
                          radius: 100,
                        ),
                        Text('Name: ${profile.Midicine}  ${profile.Properties}'),
                      ],
                    ),
                  );
                }).toList(),
              );
            }),
      ),
    );
  }
}
