import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_login/page/medicine/database/database_helper.dart';
import 'package:flutter_login/page/medicine/profile_model.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:developer' as developer;

class EditMedicine extends StatefulWidget {
  ProfileModel model;

  EditMedicine(this.model);

  @override
  _EditMedicineState createState() => _EditMedicineState(model);
}

class _EditMedicineState extends State<EditMedicine> {
  var firstname = TextEditingController();
  var lastname = TextEditingController();


  ProfileModel model;
  int? selectedId;

  var _image;
  var imagePicker;
  _EditMedicineState(this.model);

  @override
  void initState() {
    super.initState();
    imagePicker = new ImagePicker();
    this.selectedId = this.model.id;
    firstname  = TextEditingController()..text = this.model.Midicine;
    lastname = TextEditingController()..text = this.model.Properties;
    _image = this.model.image;

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellow[100],
      appBar: AppBar(
        title: Text('Edit Profile'),
        backgroundColor: Colors.purple.shade300,
      ),
      body: Container(
        color: Colors.yellow[100],
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15.15),
                      child: GestureDetector(
                        onTap: () async {
                         final image = await imagePicker.pickImage(
                              source: ImageSource.gallery,
                              imageQuality: 50,
                              preferredCameraDevice: CameraDevice.front);

                          setState(() {
                            _image = image.path;
                          });
                        },
                        child: Container(
                          width: 200,
                          height: 200,
                          decoration:
                              BoxDecoration(color: Colors.blueAccent[200]),
                          child: _image != null
                              ? Image.file(
                                  File(_image),
                                  width: 200.0,
                                  height: 200.0,
                                  fit: BoxFit.fitHeight,
                                )
                              : Container(
                                  decoration: BoxDecoration(
                                      color: Colors.blueAccent[200]),
                                  width: 200,
                                  height: 200,
                                  child: Icon(
                                    Icons.camera_alt,
                                    color: Colors.grey[800],
                                  ),
                                ),
                        ),
                      ),
                    ),
                    buildTextfield('Enter Medicine', firstname),
                    buildTextfield('Enter Properties', lastname),

                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          buildElevatedButton('Update'),
                          buildElevatedButton('Cancel'),
                        ],
                      ),
                    ),
                  ],

                ),

              ),
            ),
          ],
        ),
      ),
    );
  }

  ElevatedButton buildElevatedButton(String title) {
    return ElevatedButton(
      onPressed: () async {
        if (title == 'Update') {
              await DatabaseHelper.instance.update(
                  ProfileModel(
                      id: selectedId,
                      Midicine: firstname.text,
                      Properties: lastname.text,
                      image: _image),
                );
          setState(() {
            //firstname.clear();
            selectedId = null;
          });
          Navigator.pop(context);
        }else Navigator.pop(context);
      }
      ,
      style: ElevatedButton.styleFrom(
          primary:Colors.purple,
          fixedSize: Size(120, 50),
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          textStyle:
              const TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
      child: Text(
        title,
      ),
    );
  }

  Padding buildTextfield(String title, final ctrl) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
      child: TextField(
        controller: ctrl,
        style: TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          hintText: title,
          filled: true,
          fillColor: Colors.  white70,
          border: OutlineInputBorder(),
        ),
      ),
    );
  }

}
