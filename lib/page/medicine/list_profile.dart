

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_login/page/medicine/add_profile.dart';
import 'package:flutter_login/page/medicine/database/database_helper.dart';
import 'package:flutter_login/page/medicine/edit_profile.dart';
import 'package:flutter_login/page/medicine/profile_model.dart';
import 'package:flutter_login/page/medicine/show_profile.dart';


class ListMedicine extends StatefulWidget {
  const ListMedicine({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<ListMedicine> createState() => _ListMedicineState();
}

class _ListMedicineState extends State<ListMedicine> {

  int? selectedId;

  @override
  void initState(){
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellow[100],
      appBar: AppBar(
        title: Text(widget.title),
        backgroundColor: Colors.purple.shade300,
        actions: [
          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: IconButton(
              icon: const Icon(Icons.add),
              tooltip: 'Add Profile',
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AddMedicine()),
                ).then((value){
                  //getAllData();
                  setState(() {


                  });
                });
              },

            ),
          ),
        ],
      ),

      body: Center(
        child: FutureBuilder<List<ProfileModel>>(
            future: DatabaseHelper.instance.getProfiles(),
            builder: (BuildContext context,
                AsyncSnapshot<List<ProfileModel>> snapshot) {
              if (!snapshot.hasData) {
                return Center(child: Text('Loading...'));
              }
              return snapshot.data!.isEmpty
                  ? Center(child: Text('No Profiles in List.'))
                  : ListView(
                children: snapshot.data!.map((grocery) {
                  return Center(
                    child: Card(
                      color: selectedId == grocery.id
                          ? Colors.white70
                          : Colors.white,
                      child: ListTile(
                        title: Text('${grocery.Midicine} ${grocery.Properties}'),
                        subtitle: Text(grocery.Midicine),
                        leading: CircleAvatar(backgroundImage: FileImage(File(grocery.image))),
                        //leading: Image(
                        //  image: FileImage(File(grocery.image)),
                        //  fit: BoxFit.cover,
                        //  height: 500,
                        //  width: 100,
                        //),
                        trailing: Row(
                          mainAxisSize: MainAxisSize.min,
                          children:  <Widget>[
                            new IconButton(
                              padding: EdgeInsets.all(0),
                              icon: Icon(Icons.edit),
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => EditMedicine(grocery)),
                                ).then((value){
                                  setState(() {
                                  });
                                });
                              },
                            ),
                            new IconButton(
                              padding: EdgeInsets.all(0),
                              icon: Icon(Icons.clear),
                              onPressed: () {
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      title: new Text("Do you want to delete this record?"),
                                      // content: new Text("Please Confirm"),
                                      actions: [
                                        new TextButton(
                                          onPressed: () {
                                            DatabaseHelper.instance.remove(grocery.id!);
                                            setState(() {
                                              Navigator.of(context).pop();
                                            });
                                          },
                                          child: new Text("Ok"),
                                        ),
                                        Visibility(
                                          visible: true,
                                          child: new TextButton(
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                            child: new Text("Cancel"),
                                          ),
                                        ),
                                      ],
                                    );
                                  },
                                );

                              },
                            ),
                          ],
                        ),
                        onTap: () {
                          var profileid = grocery.id;
                          Navigator.push(context, MaterialPageRoute(builder: (context) => ShowProfile(id: profileid)));

                          setState(() {
                            print(grocery.image);
                            if (selectedId == null) {
                              //firstname.text = grocery.firstname;
                              selectedId = grocery.id;
                            } else {
                              // textController.text = '';
                              selectedId = null;
                            }
                          });
                        },
                        onLongPress: () {
                          setState(() {
                            DatabaseHelper.instance.remove(grocery.id!);
                          });
                        },
                      ),
                    ),
                  );
                }).toList(),
              );
            }),
      ),
    );
  }
}