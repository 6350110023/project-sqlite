import 'package:flutter/material.dart';
import 'package:flutter_login/page/home_page.dart';
import 'package:flutter_login/src/about.dart';
import 'package:flutter_login/src/login.dart';

class Homepage extends StatelessWidget {
  const Homepage({Key? key, required String title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellow[100],
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Image(image: AssetImage("assets/images/welcome.png")),
                    ),
                  ],
                ),
              ),


              Image.asset("assets/images/one.png"),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: SizedBox(
                  width: 250,
                  height: 40,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary:Colors.purple,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12), // <-- Radius
                      ),
                    ),
                    child: Text("Login",style: TextStyle(fontSize: 20,),),
                    onPressed: (){
                      Navigator.push(context, MaterialPageRoute(
                          builder: (context){
                            return LoginPage(title: 'page3');
                          })
                      );
                    },
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: SizedBox(
                  width: 250,
                  height: 40,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary:Colors.purple,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12), // <-- Radius
                      ),
                    ),
                    child: Text("AboutMe",style: TextStyle(fontSize: 20),),
                    onPressed: (){
                      Navigator.push(context, MaterialPageRoute(
                          builder: (context){
                            return Aboutme();
                          })
                      );
                    },
                  ),
                ),
              )
            ],
          ),
      ),
    );
  }
}
