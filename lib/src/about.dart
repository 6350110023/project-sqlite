

import 'package:flutter/material.dart';

class Aboutme extends StatelessWidget {
  const Aboutme({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellow[100],
        body: Container(
          alignment: FractionalOffset.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
              child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Image(image: AssetImage("assets/images/Team.png")),
                ),
            ],
          ),
          ),

              CircleAvatar(
                radius: 80,
                backgroundImage: AssetImage("assets/images/jom.jpg"),
              ),
              Padding(
                padding: const EdgeInsets.all(30.0),
                child:  Text(
                  "Panitan  Kuedoy ",
                  style: TextStyle(fontSize: 25),
                  textAlign: TextAlign.start,
                ),
              ),
              CircleAvatar(
                radius: 80,
                backgroundImage: AssetImage("assets/images/nat.jpg"),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 0,right: 0,top: 15,bottom: 15),
                child:  Text(
                  "Nattapat Chaiphet",
                  style: TextStyle(fontSize: 25),
                  textAlign: TextAlign.start,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 15),
                child: SizedBox(
                  width: 250,
                  height: 40,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary:Colors.purple,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12), // <-- Radius
                      ),
                    ),
                    child: Text("Back",style: TextStyle(fontSize: 20),),
                    onPressed: (){
                      Navigator.pop(context, "Hello");
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      );
  }
}
